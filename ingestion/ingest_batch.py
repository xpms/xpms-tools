import json
import os
import time
from multiprocessing import Pool
from uuid import uuid4
from minio import Minio

import boto3
import requests
from botocore.exceptions import ClientError

STORAGE = os.getenv("STORAGE", 'minio')
INGESTION_RATE = os.getenv("INGESTION_RATE", '20')
API_ENDPOINT = os.getenv("API_ENDPOINT")  # provide API endpoint to trigger text/entity extractions
POOL_TIMELIMIT = os.getenv("POOL_TIMELIMIT", '120')  # setting the dealy in seconds to wait for next round of pool documents
BATCH_TIMELIMIT = os.getenv("BATCH_TIMELIMIT", '100')  # setting the delay in seconds to wait for next round/batch
BATCH_SIZE = os.getenv("BATCH_SIZE", '200')  # tune this based on the documents count
FILE_LIMIT = None  # make it to some numeric value if needs to perform only certain count
if os.getenv("SECOND_RUN", 'False').lower() in ['false', '0']:  # switch to True if needs to re-perform the same folder to ensure all the outputs are generated
    SECOND_RUN = False
else:
    SECOND_RUN = True

bucket = os.getenv("AMAZON_AWS_BUCKET")  # provide s3/minio bucket name
folder = ""  # provide the s3 folder path
solution_id = ""  # provide the solution_id here
failed_files = []
FAILED_LIST_PATH = 'RE_PROCESS_FILES.txt'
JOB_LIST_FILE = 'JOB_IDS.txt'

if STORAGE == "s3":
    s3client = boto3.client('s3')
elif STORAGE == "minio":
    MINIO_HOST = os.getenv("MINIO_HOST")
    ACCESS_KEY = os.getenv("MINIO_ACCESS_KEY")
    SECRET_KEY = os.getenv("MINIO_SECRET_KEY")
    bucket_name = os.getenv("AMAZON_AWS_BUCKET")
    minioClient = Minio(MINIO_HOST,
                        access_key=ACCESS_KEY,
                        secret_key=SECRET_KEY,
                        secure=False)

if os.path.exists(FAILED_LIST_PATH):
    os.remove(FAILED_LIST_PATH)

if os.path.exists(JOB_LIST_FILE):
    os.remove(JOB_LIST_FILE)

data = {
    "context": {
        "ref_id": folder
    },
    "data": {
        "file_path": ""
    },
    "timeout": 600,
    "solution_id": solution_id
}

data_minio = {
    "data": {
        "inputs": {"file_path": ""}
    },
    "ref_id": "",
    "solution_id": solution_id
}


def extracted(key):
    try:
        key = os.path.splitext(key)[0] + "/output/domain_object.json"
        s3client.head_object(Bucket=bucket, Key=key)  # check if domain obj already extracted
        return True
    except ClientError as exc:
        print(key + " Not Available in s3 folder ==> " + str(exc))
        return False


def trigger_func(key):
    if key.endswith('\n'):
        key = key[:-1]
    file_path = None
    if STORAGE == "s3":
        file_path = 's3://{0}/{1}'.format(bucket, key)  # setting full s3 path with the given key
    elif STORAGE == "minio":
        file_path = 'minio://{0}/{1}'.format(bucket, key)
    data_minio["data"]["inputs"]["file_path"] = file_path
    data_minio["ref_id"] = "{0}_{1}".format(str(uuid4()), os.path.basename(key))
    try:
        print(data_minio)
        res = requests.post(url=API_ENDPOINT, data=json.dumps(data_minio))
        # if res.status_code != 200:
        print(res.__dict__, data_minio)
        if res:
            re = json.loads(res.text)
            if re.get('status').get('success'):
                job_id = re.get('job_id')
                new_job_id = job_id + "_" + str(int(time.time())) + "_" + data_minio["ref_id"]
                with open(JOB_LIST_FILE, "a") as g:
                    g.write(new_job_id + '\n')
                g.close()
                print(file_path + " => " + job_id)
            else:
                print("failure for " + file_path + " ==> " + re.get('msg'))
                return key
        else:
            print("failure none response for " + file_path)
            return key

        time.sleep(POOL_TIMELIMIT)

    except Exception as e:
        print("Failed to trigger api.." + "#" + file_path + ":" + str(e))
        return key


def trigger_batch_extract():
    pool = Pool(INGESTION_RATE)
    keys_obj = GetKeys()
    keys = keys_obj.run()  # getting total num of keys in given folder
    print("Total Files ===> " + str(len(keys)))
    total_files = len(keys)
    msg_json = {'first_file': None, 'last_file': None}
    batch_no = 0
    if total_files > BATCH_SIZE:
        msg_json['first_file'], msg_json['last_file'] = 1, BATCH_SIZE
        page_counter = 1
        while page_counter <= total_files:
            batch_no += 1
            print("Initiating batch_no >>> ", batch_no)
            results = pool.map(trigger_func, keys[msg_json['first_file'] - 1:msg_json['last_file']])
            print(results)
            page_counter = msg_json['last_file'] + 1
            msg_json['first_file'] = msg_json['last_file'] + 1
            if msg_json['last_file'] + BATCH_SIZE <= total_files:
                msg_json['last_file'] = msg_json['last_file'] + BATCH_SIZE
            else:
                msg_json['last_file'] = total_files

            if results:
                with open(FAILED_LIST_PATH, "a") as f:
                    for line in results:
                        if line is not None:
                            f.write(line + '\n')
                f.close()
            time.sleep(BATCH_TIMELIMIT)
    else:
        print("Initiating single/final batch ..!")
        results = pool.map(trigger_func, keys)
        if results:
            with open(FAILED_LIST_PATH, "a") as f:
                for line in results:
                    if line is not None:
                        f.write(line + '\n')
                f.close()

    # Re-triggering failed published files
    failed_data = open(FAILED_LIST_PATH, 'r').readlines()
    if failed_data:
        print("Failed files re-processing ==> ", failed_data, len(failed_data))
        pool.map(trigger_func, failed_data)

    return True


class GetKeys:

    def __init__(self):
        self.storage = STORAGE
        self.bucket = bucket
        self.folder = folder

    def get_matching_s3_keys(self):
        """
        Generate the keys in an S3 bucket.

        :param bucket: Name of the S3 bucket.
        :param prefix: Only fetch keys that start with this prefix (optional).
        :param suffix: Only fetch keys that end with this suffix (optional).
        """
        kwargs = {'Bucket': self.bucket, 'Prefix': self.folder, 'Delimiter': '/'}
        total_files = []
        while True:
            result = s3client.list_objects_v2(**kwargs)
            for o in result.get('Contents'):
                if o['Key'].endswith('/'):
                    continue
                if SECOND_RUN:
                    if extracted(o.get('Key')):
                        continue
                total_files.append(o.get('Key'))
                if FILE_LIMIT is not None and FILE_LIMIT in range(len(total_files)):
                    return total_files[:FILE_LIMIT]
            try:
                kwargs['ContinuationToken'] = result['NextContinuationToken']
            except KeyError:
                break
        return total_files

    def get_minio_keys(self):
        total_files = []
        list_objects = minioClient.list_objects(bucket_name, self.folder)
        for obj in list_objects:
            if obj.object_name.endswith("/"):
                continue
            total_files.append(obj.object_name)
            if FILE_LIMIT is not None and FILE_LIMIT in range(len(total_files)):
                return total_files[:FILE_LIMIT]
        return total_files

    def run(self):
        if self.storage == "s3":
            return self.get_matching_s3_keys()
        elif self.storage == "minio":
            return self.get_minio_keys()


if __name__ == '__main__':
    trigger_batch_extract()
